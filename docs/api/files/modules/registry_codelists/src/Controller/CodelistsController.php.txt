<?php
/**
 * @file
 * Contains \Drupal\registry_codelists\Controller\CodelistsController.
 */
namespace Drupal\registry_codelists\Controller;


class CodelistsController {
  

  
  
  /*
   * Start Class for creating a Codelist Response.
   */
  public function publishOutput($codelist, $code = null) {
    $format = strtolower($_GET['format']);
    $language = strtolower($_GET['language']);
    $version = strtolower( $_GET['version']);
    $response = new CodelistResponse($codelist, $code, $format , $language, $version);
    return $response->getResponse();
 
  }

}
